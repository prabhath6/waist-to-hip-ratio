(ns waist-to-hip-ratio.core
    (:require
      [reagent.core :as r]))

;; -------------------------
;; Views
(def ratio-data (r/atom {:waist 5 :hip 4}))

(defn compute-waist-to-hip-ratio
  "Takes in waist and hip sizes and computes the ratio"
  []
  (let [{:keys [waist hip ratio] :as data} @ratio-data
        ratio-data-value (/ (* 1. waist) hip)]
    (if (nil? ratio)
    (assoc data :ratio ratio-data-value))))

(defn input-elements
  "Takes user inputs for waist and hip"
  [param value]
  [:input {
           :type "text"
           :value value
           :on-change (fn [e]
                        (swap! ratio-data assoc param (.. e -target -value))
                        (swap! ratio-data assoc :ratio nil))}])

(defn ratio-component
  []
  (let [{:keys [waist hip ratio]} (compute-waist-to-hip-ratio)]
    [:div
     [:div
     "Waist: " (int waist) " cm "
     [input-elements :waist waist 0]]
     [:div
      "Hip: " (int hip) " cm "
      [input-elements :hip hip 0]]
     [:div
      "Ratio: " (float ratio)]]))

(defn home-page []
  [:div [:h2 "Waist to hip ratio"]
   [ratio-component]])

;; -------------------------
;; Initialize app

(defn mount-root []
  (r/render [home-page] (.getElementById js/document "app")))

(defn init! []
  (mount-root))
