(ns ^:figwheel-no-load waist-to-hip-ratio.dev
  (:require
    [waist-to-hip-ratio.core :as core]
    [devtools.core :as devtools]))


(enable-console-print!)

(devtools/install!)

(core/init!)
