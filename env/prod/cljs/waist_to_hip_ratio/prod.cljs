(ns waist-to-hip-ratio.prod
  (:require
    [waist-to-hip-ratio.core :as core]))

;;ignore println statements in prod
(set! *print-fn* (fn [& _]))

(core/init!)
