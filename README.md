## Waist to hip Ratio
Work out your waist-to-hip ratio (WHR) with this calculator tool. Simply enter your waist and hip measurements (in either cm or inches). Your WHR number can help you determine which body shape category you fit into.
[Reference](https://www.thecalculatorsite.com/health/whr-calculator.php)

### Development mode

To start the Figwheel compiler, navigate to the project folder and run the following command in the terminal:

```
lein figwheel
```

Figwheel will automatically push cljs changes to the browser.
Once Figwheel starts up, you should be able to open the `public/index.html` page in the browser.


### Building for production

```
lein clean
lein package
```
